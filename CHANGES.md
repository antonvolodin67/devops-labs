## ЛР 1. Airflow + docker compose

- Добавлен датасет
- Сделан Dockerfile и docker-compose.yaml
- Написан dag.py

## ЛР 2. Airflow + Spark

- Изменен Dockerfile для работы Spark
- В docker-compose.yaml добавлены spark-master и spark-worker
- Добавлен скрипт spark/count_class_occurrences.py 
- Изменен DAG, теперь подсчет представителей каждого класса выполняется через Spark

## ЛР 3. Gitlab CI/CD

- Добавлен стейдж test и соответствующая джоба, которая выполняет проверку папок spark и dags
- Добавлен стейдж build и соответствующая джоба, которая производит билд, при этом не выполняется автоматически, если ветка начинается с feature/
- Добавлен стейдж deploy и соответствующая джоба, которая выполняет деплой только из веток main, master и develop
- Созданный раннер может использоваться только для теггированых джоб
