# devops-labs



## ЛР 1. Airflow + docker compose
Задача: Развернуть Apache Airflow с помощью docker-compose и разработать свой DAG.

DAG загружает данные, считает количество представителей каждого класса, минимум и максимум для признаков.

Запуск: 
`docker-compose up -d`

## ЛР 2. Airflow + Spark
Задача: Подключить Airflow к Apache Spark и выполнить новый DAG через SparkSession.

Теперь количество представителей каждого класса считается с помощью pyspark.

Запуск (необходимо предварительно настрить подключение к Spark в админке): 
`docker-compose up -d`

## ЛР 3. Gitlab CI/CD
Задача: Создать и настроить свой первый пайплайн в Gitlab.

Добавлен пайплайн со стейджами test, build и deploy

## ЛР 4. Loki + Zabbix + Grafana
Задача: Подключить к Airflow и Spark мониторинг + логирование. Осуществить визуализацию через Grafana

![plot](grafana_screenshot.png)
