from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from airflow.utils.dates import days_ago
from datetime import timedelta
import pandas as pd
import logging


default_args = {
    'owner': 'Anton',
    'start_date': days_ago(1),
}

dag = DAG(
    'iris_dataset_processing',
    default_args=default_args,
    description='Process Iris dataset',
    schedule_interval=timedelta(hours=12),
)


def read_iris_data(**kwargs):
    iris_df = pd.read_csv('/opt/airflow/data/iris.csv').drop(columns=['Id'])
    return iris_df


read_iris_data_task = PythonOperator(
    task_id='read_iris_data',
    python_callable=read_iris_data,
    provide_context=True,
    dag=dag,
)


count_class_occurrences_task = SparkSubmitOperator(task_id='count_class_occurrences',
                                                   application=f'/opt/airflow/spark/count_class_occurrences.py',
                                                   name='count_class_occurrences',
                                                   conn_id='spark_local',
                                                   dag=dag)


def count_column_max(**kwargs):
    ti = kwargs['ti']
    iris_df = ti.xcom_pull(task_ids='read_iris_data')
    column_max = iris_df.drop(columns=['Species']).max()
    logging.info('Column max:')
    logging.info(column_max)
    return column_max.to_json()


count_column_max_task = PythonOperator(
    task_id='count_column_max',
    python_callable=count_column_max,
    provide_context=True,
    dag=dag,
)


def count_column_min(**kwargs):
    ti = kwargs['ti']
    iris_df = ti.xcom_pull(task_ids='read_iris_data')
    column_min = iris_df.drop(columns=['Species']).min()
    logging.info('Column min:')
    logging.info(column_min)
    return column_min.to_json()


count_column_min_task = PythonOperator(
    task_id='column_min',
    python_callable=count_column_min,
    provide_context=True,
    dag=dag,
)

read_iris_data_task >> count_class_occurrences_task
read_iris_data_task >> count_column_min_task
read_iris_data_task >> count_column_max_task
