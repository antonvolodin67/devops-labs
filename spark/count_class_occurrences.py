from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import count
import pandas as pd


conf = SparkConf().setAppName("Count class occurrences").setMaster("spark://spark-master:7077")
sc = SparkContext(conf=conf)
spark = SparkSession.builder.config(conf=conf).getOrCreate()
iris_df = pd.read_csv('/opt/airflow/data/iris.csv')
iris_df_spark = spark.createDataFrame(iris_df)
class_counts = iris_df_spark .groupBy("Species").agg(count("Species").alias("Count"))
class_counts.show()
spark.stop()
