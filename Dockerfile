FROM apache/airflow:2.7.2

WORKDIR /opt/airflow

USER root
RUN apt update && apt -y install procps default-jre

USER airflow
COPY ./dags/* ./dags/
COPY ./data/* ./data/
COPY ./spark/* ./spark/

RUN pip install apache-airflow-providers-apache-spark
